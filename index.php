<?php require './includes/header.php'; ?>

<div class="container" style="margin-bottom:200px">
  <div class="row">
    <div class="col-sm-3">
      <h5>.self</h5>
      <div class="profile_img">
	    <img src="./images/image1.jpg" class="img-thumbnail">
      </div>
      <br>
    </div>
    <div class="col-sm-8">
	  <h5># Hello. I am Nick.</h5>
	  <p>
	  I am a 29 year old software engineer living in Durham, NC.
	  </p>    
	  <br>
      <h5># Challenges of building a good website, with content</h5>
      <p>
      I wanted to build a personal site as a hub to show off my personal projects to prospective
      employers as well as friends and colleagues. I have and continue to encounter many challenges,
      mostly as they relate to the design of the website. What makes a website a GOOD website or even
      a GREAT website? I think a lot of it boils down to not only content, but how you organize that
      content. How smoothly does the website flow, from one page to the next? Much like you would
      want your flow controls to make logical sense, the same principles can be applied to the 
      flow of your personal site.
      </p>
    </div>
      <br>
  </div>
</div>

<?php require './includes/footer.php'; ?>
