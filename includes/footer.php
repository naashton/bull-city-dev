<div class="jumbotron text-center">
    <footer class="container-fluid">
    		<p>&copy;
    		<?php
    			date_default_timezone_set("America/New_York");
    			$startYear = 2018;
    			$thisYear = date('Y');
    			if ($startYear == $thisYear) {
    				echo $startYear;
    			} else {
    				echo "$startYear".'&ndash;'."$thisYear";
    			}
    		?>
    		Bull City Dev</p>
    </footer>
</div>

</body>
</html>