<?php session_start(); ?>
<!DOCTYPE HTML>
<html lang="en">
<head>
  <title>Bull City Dev // Nick Ashton</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
  <link href="styles/public.css" rel="stylesheet" type="text/css">
</head>
<body>

    <?php require './includes/menu.php'; ?>
    <div class="jumbotron text-center">
      <h4>Nick Ashton // Software Engineer</h4>
    </div>